import React, { useState, useMemo } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './components/layout/Navbar'
import Index from './components/layout/Index'
import {Provider} from './context';
import { UserContext } from './UserContext'
import { LoginForm } from "./components/common/LoginForm";
import './App.css';

const App =()=> {

    return (
      <Provider>
        <Router>
          <React.Fragment>
            <Navbar/>
            <UserContext.Provider>
            <LoginForm  />
            </UserContext.Provider >
            <div className="container">
              <Switch>
                <Route exact path="/" component={Index} />
              </Switch>
            </div>
          </React.Fragment>

        </Router>
      </Provider>
    )
 
}

export default App;
