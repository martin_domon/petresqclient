import React, { Component } from 'react';
import {Consumer} from '../../context'
import Post from '../posts/Post'


class Posts extends Component {
    render() {
        return (
            <Consumer>
           {value=>{


               console.log(value.posts_list);
               if(value ===undefined || value.length ===0){
                   console.log("no posts yet")

               }else{

                return(
                    <React.Fragment>
                        <h3 className="text-center mb-4">{value.heading}</h3>
                    <div className="row">
                        {value.posts_list.map(item=>(
                            
                            <Post key={item.id} post={item}/>

                        ))}

                    </div>
                    </React.Fragment>
                );

               }

           }}
            </Consumer>
        )
    }
}

export default Posts;
