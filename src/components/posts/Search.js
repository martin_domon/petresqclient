import React, { Component } from 'react'
import axios from 'axios';
import {Consumer} from '../../context'

class Search extends Component {
    state = {
        searchTerm:''

    };

formSubmit = (e)=>{
    e.preventDefault();
    axios.get(`http://localhost:8080/posts/findPost/${this.state.searchTerm}`)
    .then(res=>{
        console.log(res.data);
    
    
    })
    .catch(err=>console.log(err))

}


onChange=(e)=>{
    this.setState({[e.target.name] : e.target.value});;
}



    render() {
        return (
        <Consumer>
            {value=>{
                return (
                   <div className="card card-body mb-4 p-4">
                       <h1 className="display-4 test-center">
                           search for a pet
                       </h1>
                       <form onSubmit={this.formSubmit}>
                           <div className="form-group">
                               <input type="test" className="form-control form-control-lg"
                               placeholder="pet type"
                               name="searchTerm"
                               value={this.state.searchTerm}
                               onChange={this.onChange}></input>
                           </div>
                           <button className="btn btn-primary btn-lg btn-block mb-5" type="submit">Find</button>
                       </form>
                   </div> 
                )
            }

            }
        </Consumer>
        )
    }
}

export default Search;