import React from 'react'

const Post =(props)=> {

    console.log(props);

    return (
        <div className="col-md-6">
            <div className="card mb-4 shadow-sm">
                <div className="card-body">

    <h5>{props.post.animalName}</h5>
    <p>{props.post.type}</p>
    <p>{props.post.info}</p>

                </div>
            </div>
            
        </div>
    )
}

export default Post;