import React, { Component } from 'react'
import axios from 'axios';

const Context = React.createContext();

export class Provider extends Component {
    state = {
        posts_list: [],
        heading:"Latest"
    };

    componentDidMount(){

        axios.get(`http://localhost:8080/posts/allposts`)
        .then(res=>{
            console.log(res.data);
            this.setState({posts_list:res.data})
        
        
        })
        .catch(err=>console.log(err))


    }

    render() {
        return (
            <Context.Provider value={this.state}>
                {this.props.children}
            </Context.Provider>
        )
    }

}

export const Consumer = Context.Consumer;
